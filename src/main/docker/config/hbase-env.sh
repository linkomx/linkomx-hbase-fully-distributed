#!/usr/bin/env bash

export JAVA_HOME=/usr/lib/jvm/zulu8-ca

export HBASE_HEAPSIZE=1G

export HBASE_OFFHEAPSIZE=1G

export HBASE_OPTS="$HBASE_OPTS -XX:+UseConcMarkSweepGC"
